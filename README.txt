Tired of constantly typing git something in Microsoft PowerShell?

Then this project may be for you:

At the moment, I am attempting to use batch files which I have constantly used to make command line easier (though git is already pretty easy)
But fun right?

All the batch files will commit everything and <<< MUST BE IN THE SAME FOLDER AS YOUR GIT PROJECT >>>. 
There is a way to make them commit specifics, but too much trouble.
The files are pretty self-explanatory (especially with git status saying everything needed to do), but just in case here are the file details:

FILE DETAILS
- startPowershell.bat
Opens Microsoft Powershell within the directory this file is in
WARNING: do not rename this file to: powershell.bat it will cause an infinite loop of command prompt (kinda cool but frightening the first time)

- gitAdd.bat
calls the git add for user given files

- gitAddAll.bat
calls the git add for all changes made

- gitCommit.bat
prompts the user for the name/message that they want to call the commit before committing everything added

- gitPullAll.bat
pulls all that is not synced

- gitPush.bat
pushes all committed

- gitStatus.bat
displays the git status

- gitUpdate.bat
updates the repository by adding all, committing with a user given message and then pushing it all

- gitUpdater.bat
updates the repository by adding user given files, committing with a user given message and then pushing it all